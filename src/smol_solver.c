#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/sysinfo.h>
#include "smol_eq.h"


char count_weight = 0;
int threads_num = 0, g = 0, kernel_type = 0, source_type = 0, len = 0;
double *c = NULL, t_0 = 0, t_end = 0;
int step = 0;
pthread_barrier_t barr, varr;

struct threads_arguments{
    int *index;
    int num;
    int name;
    double (*kernel) (int k_type, int i, int j);
    double (*source) (int s_type);
};

void solver(int i, double (*kern)(int k_type, int i, int j), double (*source)(int s_type))
{
    double h = (double) 1 / step;
    double k_1 = equation(c, i, g, kernel_type, kern, 0, source_type, source);
    double k_2 = equation(c, i, g, kernel_type, kern, k_1 * h * 0.5, source_type, source);
    double k_3 = equation(c, i, g, kernel_type, kern, k_2 * h * 0.5, source_type, source);
    double k_4 = equation(c, i, g, kernel_type, kern, k_3 * h, source_type, source);

    c[i] += h * (k_1 +  2 * k_2 + 2 * k_3 + k_4) / 6; 
}

void * son_job(void *data)
{
    int res = 0;
    struct threads_arguments *arg = data;
    for(int i = 0; i < len; i++)
    {
        res = pthread_barrier_wait(&barr);

        for(int j = 0; j < arg->num; j++)
        {
            solver(arg->index[j], arg->kernel, arg->source);
        }

        res = pthread_barrier_wait(&varr);
    }

    free(arg);
    pthread_exit(0);
}

double sum(double array[], int len)
{
    double res = 0;
    for(int i = 0; i < len; i++) res += (count_weight ? (i + 1) : 1) * array[i];
    return res;
}

double source(int s_type)
{
    switch(s_type)
    {
        case 0:
            return 0;
        case 1:
            return 1;
        default:
            fprintf(stderr, "Unknown source type\n");
            return -1;
    }
}

double kernel(int type, int i, int j)
{
    double c =  1/ (double) 3;

    switch (type)
    {
        case 1:
            return 1;
        case 2:
            return i + j;
        case 3:
            return i * j;
        case 4:
            return sqrt((i + j) / (i * j)) * pow((pow(i, c) + pow(j, c)), 2);
        case 5:
            return (pow(i, c) + pow(j, c)) * (pow(i, -c) + pow(j, -c));
        default:
            fprintf(stderr, "Unknown kernel type\n");
            return -1;
    }
}

int arguements_parsing(int argc, char *argv[], FILE **in, FILE **out)
{
    if (argc > 8)       count_weight = atoi(argv[8]);
    if(argc > 7)
    {
        t_0 = strtod(argv[6], NULL);
        t_end = strtod(argv[7], NULL);
    }
    else if (argc == 7)
    {
        fprintf(stderr, "ERROR! Too few arguements!\n");
        return 1;
    }
    if(argc > 5)       step = atoi(argv[5]);
    if(argc > 4)       source_type = atoi(argv[4]);
    if(argc > 3)       kernel_type = atoi(argv[3]);
    if(argc > 2)       *out = fopen(argv[2], "w");
    if(argc > 1)       *in = fopen(argv[1], "r");
    return 0;
}

int main(int argc, char *argv[])
{
    double buff = 0, j = 0, h = 0;
    int rabbits = 0, cells = 0, num = 0, k = 0;
    FILE *in = stdin, *out = stdout;

    if(arguements_parsing(argc, argv, &in, &out))    return 1;

    while(fscanf(in, "%lf", &buff) != EOF)
    {
        c = realloc(c, sizeof(double) * ++g);
        c[g - 1] = buff;
    }

    len = (t_end - t_0) * step + 1;

    threads_num = (get_nprocs() < g) ? get_nprocs() : g;
    pthread_t tid[threads_num];

    cells = g / threads_num;
    rabbits = g % threads_num;

    pthread_barrier_init(&barr, NULL, threads_num + 1);
    pthread_barrier_init(&varr, NULL, threads_num + 1);

    for(int i = 0; i < threads_num; i++)
    {
        struct threads_arguments *arg;
        arg = malloc(sizeof(struct threads_arguments));

        arg->num = cells + ((rabbits-- > 0) ? 1 : 0);
        arg->index = malloc(sizeof(int) * arg->num);
        arg->name = i;
        arg->kernel = kernel;
        arg->source = source;
        for(int j = 0; j < arg->num; j++)       arg->index[j] = num + j;
        num += arg->num;
        pthread_create(&tid[i], NULL, son_job, arg);
    }

    h = (double) 1 / step;
    for(k = 0, j = t_0; k < len; j = t_0 + k * h, k++)
    {
        fprintf(stdout, "%.2lf%%\r", (double) k / len * 100);
        fprintf(out, "%lf ", j);
        if (count_weight == 1)       fprintf(out, "%lF", sum(c, g));
        else if (count_weight == 2)
        {
            for(int i = 0; i < g; i++)      fprintf(out, "%lF ", (i + 1) * c[i]);
        }
        else
        {
            for(int i = 0; i < g; i++)      fprintf(out, "%lg ", c[i]);
        }
        fprintf(out, "\n");
        pthread_barrier_wait(&barr);
        pthread_barrier_wait(&varr);
    }

    for(int i = 0; i < threads_num; i++)        pthread_join(tid[i], NULL);

    free(c);
    fclose(out);
    fclose(in);
    return 0;
}
