double equation(double c[], int i, int g, int ktype, double (*kernel) (int k_type, int i, int j), double growth, int s_type, double (*source) (int s_type))
{
    double sum_1 = 0, sum_2 = 0;

    for(int j = 0; j < i; j++)  sum_1 += kernel(ktype, i - j, j + 1) * c[i - j - 1] * c[j];

    for(int j = 0; j < g; j++)  sum_2 += kernel(ktype, i + 1, j + 1) * (j == i ? c[j] + growth : c[j]);

    sum_2 *= (c[i] + growth);

    return ((i == 0) ? source(s_type) : 0) + sum_1 * 0.5 - sum_2;
}
